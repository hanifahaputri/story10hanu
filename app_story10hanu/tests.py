from django.http import HttpRequest
from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from .views import *
from .forms import *

class Story10UnitTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('/')
        response2 = self.client.get('/login/')
        response3 = self.client.get('/register/')
        response4 = self.client.get('/logout/')
        response5 = self.client.get('/accounts/profile/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(response3.status_code, 200)
        self.assertEqual(response4.status_code, 302)
        self.assertEqual(response5.status_code, 302)

    def test_confirmation_page_url_template_and_function(self):
        response = self.client.get('/')
        response2 = self.client.get('/register/')

        found = resolve('/')
        found2 = resolve('/register/')

        self.assertEqual(found.func, frontpage)
        self.assertEqual(found2.func, register)

    def test_template(self):
        response = self.client.get('/')
        response2 = self.client.get('/register/')

        self.assertTemplateUsed(response, 'frontpage.html')
        self.assertTemplateUsed(response2, 'register.html')

    def test_register_user_exists(self):
        response = self.client.get('/register/')

        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')


  
