from django import forms

class registration(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username',
               'type': 'text', 'required': True}))
    email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Email', 
               'type': 'email', 'required': True}))
    password = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Password',
               'type': 'password', 'required': True}))
