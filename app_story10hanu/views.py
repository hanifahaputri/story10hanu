from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http import HttpResponse 
from django.shortcuts import redirect, render
from .forms import registration

def frontpage(request):
    return render(request, 'frontpage.html')

@login_required
def home(request):
    return render(request, 'home.html')

def register(request):
    if request.method == "POST":
        filled = registration(request.POST)
        if filled.is_valid():
            try:
                username = filled.cleaned_data['username']
                email = filled.cleaned_data['email']
                password = filled.cleaned_data['password']
                print(User.objects.create_user(username, email, password))
                
                return render(request, 'registration/login.html', {'form': filled})

            except IntegrityError:
                return HttpResponse("ERROR: That data already exists! Please try another one.")
        else:
            filled = registration()
        
    else:
        filled = registration()
        return render(request, 'register.html', {'form' : filled})